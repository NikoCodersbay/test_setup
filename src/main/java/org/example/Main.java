package org.example;

import org.example.config.AppConfig;

import org.hibernate.Session;


public class Main {
    public static void main(String[] args) {

        Session session = AppConfig.startSession();

        session.getTransaction().begin();
        System.out.println("Session Is Opened :: " + session.isOpen());
        System.out.println("Session Is Connected :: " + session.isConnected());

        session.getTransaction().commit();


    }


}